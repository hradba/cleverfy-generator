package com.cleverfy.qg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.decompose.DecomposedSentence;
import com.cleverfy.qg.decompose.Decomposer;
import com.cleverfy.qg.design.QuestionDesigner;
import com.cleverfy.qg.expand.ExpandedWord;
import com.cleverfy.qg.expand.Expander;
import com.cleverfy.qg.expand.VerbExpander;
import com.cleverfy.qg.model.QuestionDesign;
import com.cleverfy.qg.model.SentenceQuestionDesigns;
import com.cleverfy.qg.model.SentenceQuestionDesignsComparator.SentenceComparatorBuilder;
import com.cleverfy.qg.nlg.AnswerVariantRealiser;
import com.cleverfy.qg.nlg.QuestionFormatter;
import com.cleverfy.qg.nlg.QuestionRealiser;
import com.cleverfy.qg.nlg.StandardQuestionFormatter;
import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.StanfordParser;
import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.cleverfy.qg.parser.dependencies.SentenceTree;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

/**
 * Main class of system responsible for question generation. Creation of instance is time & resource-expensive operation
 * (Stanford parser initialization).
 * 
 * After obtaining an instance, generate method may be reused for unlimited number of input texts.
 * 
 * @author Radim Hradecký
 *
 */
public class QuestionGenerator {
	
	final static Logger logger = LoggerFactory.getLogger(QuestionGenerator.class);
	
	private Parser parser;
	private Decomposer decomposer;
	private QuestionDesigner questionDesigner;
	private QuestionRealiser questionRealiser;
	
	public QuestionGenerator() {
		this(new StandardQuestionFormatter(), new StanfordParser());
	}
	
	public QuestionGenerator(QuestionFormatter questionFormatter, Parser parser) {
		this.parser = parser;
		this.decomposer = new Decomposer();
		this.questionDesigner = new QuestionDesigner(new Expander(), new VerbExpander());
		this.questionRealiser = new QuestionRealiser(questionFormatter);
	}
	
	/**
	 * Generates questions for all sentences in given input text.
	 * 
	 * @param inputText text to create questions from
	 * @return questions for given input text
	 */	
	public Collection<Question> generate(String inputText) {
		Text text = Text.valueOf(inputText);
		return generate(text, -1, -1);
	}
	
	/**
	 * Generates questions for sentences within given range in given text.
	 * 
	 * @param text to create questions from
	 * @param from zero-based index of sentence to start generation from
	 * @param to zero-based index of sentence to end generation from
	 * @return questions for given input text
	 */
	public Collection<Question> generate(Text text, int from, int to) {
		
		long start = System.currentTimeMillis();
		
		// #0 a)
		SentenceTree sentenceContextTree = new SentenceTree();
		AnswerVariantRealiser answerVariantRealiser = new AnswerVariantRealiser();
		SentenceComparatorBuilder sentenceComparatorBuilder = new SentenceComparatorBuilder();
		List<SentenceQuestionDesigns> sentencesQuestionDesigns = new ArrayList<>();
		int paragraphIndex = 0;
		int sentenceIndex = 0;
		
		for (Paragraph paragraph : text.getParagraphs()) {
			ParsedText parsedText = parser.parse(paragraph.getText());
			List<SentenceTree> paragrapSentencesTrees = parsedText.getSentenceTrees();
			for (SentenceTree paragraphSentenceTree : paragrapSentencesTrees) {
				if (isQuestionGenerationAllowed(sentenceIndex, from, to)) {
					SentenceTreeNavigator sentenceNavigator = new SentenceTreeNavigator(paragraphSentenceTree);
					String context = sentenceContextTree.getText();
					answerVariantRealiser.findAlternatives(sentenceNavigator);
					sentenceComparatorBuilder.addSentence(sentenceIndex, paragraphIndex, sentenceNavigator);
					sentencesQuestionDesigns.addAll(sentenceToQuestionDesigns(sentenceNavigator, context, sentenceIndex));
					logDebugInfo(sentenceNavigator);
					sentenceContextTree = paragraphSentenceTree;
				}
				sentenceIndex++;
			}
			paragraphIndex++;
		}
		
		Collections.sort(sentencesQuestionDesigns, sentenceComparatorBuilder.build());
		Collection<Question> questions = questionDesignsToQuestions(sentencesQuestionDesigns, answerVariantRealiser);
		
		logger.info("Generating took: " + (System.currentTimeMillis() - start) + "ms");
		
		return questions;
	}
	
	private Collection<Question> questionDesignsToQuestions(List<SentenceQuestionDesigns> sentencesQuestionDesigns,
	        AnswerVariantRealiser answerVariantRealiser) {
		
		List<Question> questions = new ArrayList<>();
		int importanceOrder = 1;
		for (SentenceQuestionDesigns sentenceQuestionDesigns : sentencesQuestionDesigns) {
			List<Question> sentenceQuestions = new ArrayList<>();
			for (QuestionDesign sentenceQuestionDesign : sentenceQuestionDesigns.getQuestionDesigns()) {
				Question question = generateQuestionFromDesign(sentenceQuestionDesigns.getText(),
				        sentenceQuestionDesign, importanceOrder, answerVariantRealiser);
				sentenceQuestions.add(question);
				importanceOrder++;
			}
			logDebugInfo(sentenceQuestions);
			questions.addAll(sentenceQuestions);
		}
		return questions;
	}
	
	private List<SentenceQuestionDesigns> sentenceToQuestionDesigns(SentenceTreeNavigator sentenceNavigator,
	        String context, int sentenceIndex) {
		List<SentenceQuestionDesigns> sentencesQuestionDesigns = new ArrayList<>();
		Set<DecomposedSentence> decomposedSentences = decomposer.decompose(sentenceNavigator);
		for (DecomposedSentence decomposedSentence : decomposedSentences) {
			List<QuestionDesign> questionDesigns = questionDesigner.design(decomposedSentence, sentenceNavigator);
			sentencesQuestionDesigns.add(new SentenceQuestionDesigns(context, sentenceIndex, questionDesigns));
		}
		return sentencesQuestionDesigns;
	}
	
	private boolean isQuestionGenerationAllowed(int sentenceIndex, int from, int to) {
		return (from == -1 && to == -1) || (sentenceIndex >= from && sentenceIndex <= to);
	}
	
	/**
	 * Convenience method for testing - questions are generated only for sentences within given range
	 * 
	 * @param inputText text to create questions from
	 * @param from zero-based index of sentence to start generation from
	 * @param to zero-based index of sentence to end generation from
	 * @return questions for given input text
	 */
	public Collection<Question> generate(String inputText, int from, int to) {
		Text text = Text.valueOf(inputText);
		return generate(text, from, to);
	}
	
	private Question generateQuestionFromDesign(String context, QuestionDesign questionDesign, long importanceOrder,
	        AnswerVariantRealiser answerVariantRealiser) {
		String realisedQuestion = questionRealiser.realise(questionDesign);
		ExpandedWord answer = questionDesign.getAnswer();
		Collection<String> answerVariants = answerVariantRealiser.realiseAnswerVariants(answer);
		return new Question(context, realisedQuestion, answer.text(), importanceOrder, answerVariants);
	}
	
	private void logDebugInfo(SentenceTreeNavigator navigator) {
		StringBuilder sb = new StringBuilder();
		List<Word> words = navigator.getWords();
		
		for (Word word : words) {
			sb.append(word.getText() + " " + word.getPOS() + " " + word.getPOS() + "|");
		}
		logger.debug(sb.toString());
		logger.debug("Parse tree:\n" + navigator.format());
		logger.debug("Parse sentence tree:\n" + navigator.formatTargetSentenceTree());
		
	}
	
	private void logDebugInfo(Collection<Question> questions) {
		StringBuilder sb = new StringBuilder();
		sb.append("Generated questions (" + questions.size() + "):");
		for (Question question : questions) {
			sb.append(question.prettyPrint());
		}
		logger.debug(sb.toString());
		
	}
	
}
