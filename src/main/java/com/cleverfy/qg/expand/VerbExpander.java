package com.cleverfy.qg.expand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cleverfy.qg.parser.dependencies.POS;
import com.cleverfy.qg.parser.dependencies.Relation;
import com.cleverfy.qg.parser.dependencies.RelationType;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

public class VerbExpander {

	public ExpandedVerbWord expandVerbWord(Word verb, Word subject, SentenceTreeNavigator navigator) {
		Word mainVerb = verb;		
		List<Word> auxVerbs = getAuxVerbs(mainVerb, navigator);
		Word mainAuxVerb = getMainAuxVerb(auxVerbs);				
		return new ExpandedVerbWord(mainVerb, mainAuxVerb, auxVerbs, subject.inPlural());
	}

	private static final List<RelationType> AUX_VERB_RELATIONS = Arrays.asList(RelationType.AUX_MODIFIER, RelationType.AUX_PASSIVE_MODIFIER, RelationType.VERBAL_MODIFIER, RelationType.PHRASAL_VERB_PARTICLE);
	
	private List<Word> getAuxVerbs(Word mainVerb, SentenceTreeNavigator navigator) {
		List<Relation> auxRelations = mainVerb.getChildrenWithRelation(AUX_VERB_RELATIONS);
		List<Word> auxVerbs = new ArrayList<>();
		for (Relation relation: auxRelations) {
			auxVerbs.add(relation.getTarget());
		}
		return auxVerbs;
	}

	private Word getMainAuxVerb(List<Word> auxVerbs) {
		
		// priorities - Modal, aux to have, aux to be
		for (Word word : auxVerbs) {
			if (word.getPOS() == POS.MD) {
				return word;
			}
		}
		for (Word word : auxVerbs) {
			if (word.getLemma().equals("have")) {
				return word;
			}
		}
		for (Word word : auxVerbs) {
			if (word.getLemma().equals("be")) {
				return word;
			}
		}
		for (Word word : auxVerbs) {
			if (word.getLemma().equals("do")) {
				return word;
			}
		}
		
		return null;
	}

	
	

}

/*
public static VerbWords expandVerb(IndexedWord verbRoot, IndexedWord subject, IndexedWord object,
        SemanticGraph deps, Set<IndexedWord> excludes) {
	
	IndexedWord mainVerb = getMainVerb(verbRoot, deps);
	
	Set<IndexedWord> auxVerbs = getAuxVerbs(verbRoot, deps);
	IndexedWord mainAuxVerb = getMainAuxVerb(auxVerbs);
	
	VerbWords verbWords = VerbWords.create(isPastTense(mainVerb, mainAuxVerb) ? Tense.PAST : Tense.PRESENT,
	        isNounPlural(subject), isNegation(verbRoot, deps));
	verbWords = verbWords.addMainVerb(mainVerb, true);
	for (IndexedWord auxVerb : auxVerbs) {
		if (auxVerb == mainAuxVerb) {
			verbWords = verbWords.addMainAuxVerb(mainAuxVerb);
		} else {
			verbWords = verbWords.addAuxVerb(auxVerb);
		}
	}
	
	return verbWords.build();
}

public static boolean isNounPlural(IndexedWord noun) {
	if (noun != null && noun.get(LemmaAnnotation.class) != null
	        && noun.get(LemmaAnnotation.class).equals(noun.get(OriginalTextAnnotation.class))) {
		return false;
	} else {
		return true;
	}
}

public static IndexedWord getMainVerb(IndexedWord root, SemanticGraph dependencies) {
	Set<IndexedWord> children = dependencies.getChildrenWithReln(root, EnglishGrammaticalRelations.COPULA);
	if (children.size() == 0) {
		return root;
	} else {
		return children.iterator().next();
	}
}

public static Set<IndexedWord> getAuxVerbs(IndexedWord root, SemanticGraph dependencies) {
	
	Set<IndexedWord> auxs = dependencies.getChildrenWithRelns(root, Arrays.asList(
	        EnglishGrammaticalRelations.AUX_MODIFIER, EnglishGrammaticalRelations.AUX_PASSIVE_MODIFIER,
	        EnglishGrammaticalRelations.VERBAL_MODIFIER, EnglishGrammaticalRelations.PHRASAL_VERB_PARTICLE));
	return auxs;
}

public static boolean isNegation(IndexedWord root, SemanticGraph dependencies) {
	Set<IndexedWord> auxs = dependencies.getChildrenWithRelns(root,
	        Arrays.asList(EnglishGrammaticalRelations.NEGATION_MODIFIER));
	return auxs.size() > 0;
}

public static IndexedWord getMainAuxVerb(Set<IndexedWord> auxVerbs) {
	
	// Set<IndexedWord> auxs = dependencies.getChildrenWithRelns(root, Arrays.asList(
	// EnglishGrammaticalRelations.AUX_MODIFIER, EnglishGrammaticalRelations.AUX_PASSIVE_MODIFIER));
	// priorities - Modal, aux to have, aux to be
	for (IndexedWord word : auxVerbs) {
		if (POS.valueOf(word.getString(PartOfSpeechAnnotation.class)) == POS.MD) {
			return word;
		}
	}
	for (IndexedWord word : auxVerbs) {
		if (word.getString(LemmaAnnotation.class).equals("have")) {
			return word;
		}
	}
	for (IndexedWord word : auxVerbs) {
		if (word.getString(LemmaAnnotation.class).equals("be")) {
			return word;
		}
	}
	for (IndexedWord word : auxVerbs) {
		if (word.getString(LemmaAnnotation.class).equals("do")) {
			return word;
		}
	}
	
	return null;
}

public static boolean isPastTense(IndexedWord mainVerb, IndexedWord mainAuxVerb) {
	POS mainVerbPOS = POS.valueOf(mainVerb.getString(PartOfSpeechAnnotation.class));
	if (mainVerbPOS.isPastVerb()) {
		return true;
	}
	if (mainAuxVerb != null) {
		POS mainAuxVerbPOS = POS.valueOf(mainAuxVerb.getString(PartOfSpeechAnnotation.class));
		if (mainAuxVerbPOS.isPastVerb()) {
			return true;
		}
	}
	
	return false;
}*/

