package com.cleverfy.qg.expand;

import java.util.ArrayList;
import java.util.List;

import com.cleverfy.qg.parser.dependencies.Relation;
import com.cleverfy.qg.parser.dependencies.RelationType;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

public class Expander {
	
	public ExpandedWord expandWord(Word rootWord, SentenceTreeNavigator navigator) {
		return new ExpandedWord(rootWord, expandWord(rootWord, rootWord, new ArrayList<Word>(), navigator));
	}
	
	public ExpandedWord expandWords(List<Word> rootWords, SentenceTreeNavigator navigator) {
		List<Word> expanded = new ArrayList<>();
		for (Word rootWord: rootWords) {
			expanded.addAll(expandWord(rootWord, rootWord, new ArrayList<Word>(), navigator));
		}
		return new ExpandedWord(null, expanded);
	}
	
	private List<Word> expandWord(Word rootWord, Word current, List<Word> expanded, SentenceTreeNavigator navigator) {
		if (current == null) return new ArrayList<>();
		List<Relation> children = current.getChildren();		
		expanded.add(current);
		
		Word prevPunct = null;
		boolean lastWordSkipped = false;
		
		childLoop: for (Relation childRelation : children) {
			Word child = childRelation.getTarget();
			// #6
			List<Relation> objects = child.getChildrenWithRelation(RelationType.PREPOSITIONAL_OBJECT);
			for (Relation relation: objects) {
				if (relation.getTarget().getPOS().isVerb()) continue childLoop;
			}
			// #11
			if (/*child.getPOS().isUnknown() || */!child.getPOS().isVerb() || (child.getPOS().isVerb() && childRelation.getRelationType() == RelationType.ADJECTIVAL_MODIFIER)) {
				if (prevPunct != null) {
					expandWord(rootWord, prevPunct, expanded, navigator);
					prevPunct = null;
				}
				
				// #10
				if (childRelation.getRelationType() == RelationType.PUNCTUATION) {
					prevPunct = child;
				} else {
					expandWord(rootWord, child, expanded, navigator);	
				}
				lastWordSkipped = false;
			} else {
				prevPunct = null;
				lastWordSkipped = true;
			}			
		}
		if (prevPunct != null && !lastWordSkipped) {
			expandWord(rootWord, prevPunct, expanded, navigator);
		}
		return expanded;
	}
	
	
}
