package com.cleverfy.qg.expand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.cleverfy.qg.parser.dependencies.Word;

public class ExpandedVerbWord {
	
	private Word mainVerb;	
	private List<Word> expanded;	
	private Word mainAuxVerb;
	private Tense tense;
	private boolean plural;
	private boolean negation;
	
	public ExpandedVerbWord(Word mainVerb, Word mainAuxVerb, List<Word> auxVerbs, boolean plural) {
		this.mainVerb = mainVerb;
		this.mainAuxVerb = mainAuxVerb;
		this.negation = mainVerb.hasNegationModifier();
		this.plural = plural;
		this.tense = mainVerb.getPOS().isPastVerb() || (mainAuxVerb != null && mainAuxVerb.getPOS().isPastVerb()) ? Tense.PAST : Tense.PRESENT;
		expanded = new ArrayList<>();
		expanded.addAll(auxVerbs);
		if (mainVerb != null) expanded.add(mainVerb);
		//if (mainAuxVerb != null) expanded.add(mainAuxVerb);
		Collections.sort(expanded, new VerbWordComparator());
	}
	
	public enum Tense {
		PAST,
		PRESENT,
		FUTURE
	}
	
	public String text(boolean mainVerbToLemma, boolean skipMainVerb) {
		List<Word> verbWords = getExpanded();
		StringBuilder sb = new StringBuilder();		
		Iterator<Word> iterator = verbWords.iterator();
		while (iterator.hasNext()) {
			Word verbWord = iterator.next();
			if (verbWord.equals(getMainAuxVerb())) {
				continue;
			}
			if (verbWord.equals(getMainVerb()) && skipMainVerb) {
				continue;
			}
			if (verbWord.equals(getMainVerb()) && mainVerbToLemma) {				
				sb.append(verbWord.getLemma());
			} else {
				sb.append(verbWord.getText());	
			}			
			if (iterator.hasNext()) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
	public Word getMainVerb() {
		return mainVerb;
	}

	public List<Word> getExpanded() {
		return Collections.unmodifiableList(expanded);
	}
	
	
	public boolean isMainVerbThirdPerson() {
		//TODO fix
		return true;
	}
	
	public boolean isNegation() {
		return negation;
	}
	
	public boolean hasOwnMainAuxVerb() {
		return mainAuxVerb != null;	
	}

	
	public boolean isPlural() {
		return plural;
	}
	
	public Tense getTense() {
		return tense;
	}

	
	public Word getMainAuxVerb() {
		return mainAuxVerb;
	}	
	

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	static class VerbWordComparator implements Comparator<Word> {

		@Override
        public int compare(Word o1, Word o2) {	      
			if (o1 == null || o2 == null) return 0;
	        return o1.getBeginOffset().compareTo(o2.getBeginOffset());
        }
		
	}

}
