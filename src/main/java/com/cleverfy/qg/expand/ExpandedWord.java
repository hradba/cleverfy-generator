package com.cleverfy.qg.expand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.cleverfy.qg.parser.dependencies.Word;

public class ExpandedWord {
	
	private Word root;	
	private List<Word> expanded;
	
	public ExpandedWord(Word root, List<Word> expanded) {
	    super();
	    this.root = root;
	    this.expanded = expanded;
    }

	public Word getRoot() {
		return root;
	}

	public List<Word> getExpanded() {
		return Collections.unmodifiableList(expanded);
	}
	
	public String text() {
		return text(new HashMap<Word, String>());
	}
	
	public ExpandedWord merge(ExpandedWord word) {
		List<Word> merged = new ArrayList<>(expanded);
		merged.addAll(word.getExpanded());
		return new ExpandedWord(root, merged);
	}
	
	public String text(Map<Word, String> changes) {
		
		Collections.sort(expanded);
		
		Iterator<Word> it = expanded.iterator();
		StringBuilder sb = new StringBuilder();
		boolean afterWasEmpty = false;
		while (it.hasNext()) {
			Word curr = it.next();
			if (afterWasEmpty) {
				sb.append(curr.getBefore());
				afterWasEmpty = false;
			}
			if (!changes.containsKey(curr)) {
				sb.append(curr.getText());				
			} else {
				sb.append(changes.get(curr));
					
			}						
			if (it.hasNext()) {
				String after = curr.getAfter();
				if (after.length() == 0) afterWasEmpty = true;
				sb.append(after);
			}
		}
		
		return sb.toString();		
	}
	
	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}
}
