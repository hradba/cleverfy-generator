package com.cleverfy.qg;

import java.util.List;

/**
 * Class of representation of input text paragraph as a logical unit of text. Paragraph position and highligths of it's contents have impact on question order.
 * 
 * @author Radim Hradecký
 *
 */
public class Paragraph {
	
	private String text;
	private List<HighlightRange> highlights;

	public Paragraph(String text, List<HighlightRange> highlights) {
	    super();
	    this.text = text;
	    this.highlights = highlights;
    }
	
	
	public boolean isHighlighted(long startIndex, long endIndex) {
		for (HighlightRange highlightRange: highlights) {
			if (startIndex <= highlightRange.getStart() && endIndex >= highlightRange.getEnd()) return true;
		}
		return false;
	}


	public String getText() {
		return text;
	}
	
}
