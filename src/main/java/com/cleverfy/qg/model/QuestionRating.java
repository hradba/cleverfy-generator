package com.cleverfy.qg.model;

public class QuestionRating {

	private boolean unresolvedCoreferences;
	private int questionLength;
	private int answerLength;
	
	private QuestionRating(boolean unresolvedCoreferences, int questionLength, int answerLength) {
	    super();
	    this.unresolvedCoreferences = unresolvedCoreferences;
	    this.questionLength = questionLength;
	    this.answerLength = answerLength;
    }
	
	public boolean isUnresolvedCoreferences() {
		return unresolvedCoreferences;
	}

	public int getQuestionLength() {
		return questionLength;
	}

	public int getAnswerLength() {
		return answerLength;
	}
	
	
}
