package com.cleverfy.qg.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.cleverfy.qg.expand.ExpandedVerbWord;
import com.cleverfy.qg.expand.ExpandedWord;
import com.cleverfy.qg.nlg.QuestionType;

public class QuestionDesign {

	private int matchId;
	private QuestionType questionType;
	private QuestionTarget questionTarget;
	private ExpandedWord subject;
	private ExpandedWord object;
	private ExpandedVerbWord verb;
	private ExpandedWord ext;
	private String endingPrep;

	public QuestionDesign(int matchId, QuestionType questionType, QuestionTarget questionTarget, ExpandedWord subject, ExpandedWord object, ExpandedVerbWord verb, String endingPrep) {
	    super();
	    this.matchId = matchId;
	    this.questionType = questionType;
	    this.questionTarget = questionTarget;
	    this.subject = subject;
	    this.object = object;
	    this.verb = verb;
	    this.endingPrep = endingPrep;
    }

	
	
	
	public QuestionDesign(int matchId, QuestionType questionType, QuestionTarget questionTarget, ExpandedWord subject, ExpandedWord object, ExpandedVerbWord verb, ExpandedWord ext, String endingPrep) {
	    super();
	    this.matchId = matchId;
	    this.questionType = questionType;
	    this.questionTarget = questionTarget;
	    this.subject = subject;
	    this.object = object;
	    this.verb = verb;
	    this.ext = ext;
	    this.endingPrep = endingPrep;
    }




	public int getMatchId() {
		return matchId;
	}


	public QuestionType getQuestionType() {
		return questionType;
	}

	
	public QuestionTarget getQuestionTarget() {
		return questionTarget;
	}


	public ExpandedWord getSubject() {
		return subject;
	}

	public ExpandedWord getObject() {
		return object;
	}

	public ExpandedVerbWord getVerb() {
		return verb;
	}

	
	public ExpandedWord getExt() {
		return ext;
	}




	public String getEndingPrep() {
		return endingPrep;
	}




	public ExpandedWord getAnswer() {
		return (questionTarget == QuestionTarget.OBJECT) ? object : subject;
	}


	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}

}
