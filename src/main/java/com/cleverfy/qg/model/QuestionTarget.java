package com.cleverfy.qg.model;

public enum QuestionTarget {

	SUBJECT,
	OBJECT
}
