package com.cleverfy.qg.model;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

public class SentenceQuestionDesignsComparator implements Comparator<SentenceQuestionDesigns> {
	
	private SortedSet<String> sortedLemmas;
	private Map<Integer, Set<String>> sentenceIncludedWords = new HashMap<>();
	int lastParagraphIndex = 0;
	
	
	
	public SentenceQuestionDesignsComparator(SortedSet<String> sortedLemmas, Map<Integer, Set<String>> sentenceIncludedWords,
            int lastParagraphIndex) {
	    super();
	    this.sortedLemmas = sortedLemmas;
	    this.sentenceIncludedWords = sentenceIncludedWords;
	    this.lastParagraphIndex = lastParagraphIndex;
    }

	public static class SentenceComparatorBuilder {
		private Map<String, Integer> wordCounts = new HashMap<>();
		private Map<Integer, Set<String>> sentenceIncludedWords = new HashMap<>();
		
		private int lastParagrapIndex = 0;
		
		public SentenceComparatorBuilder addSentence(int sentenceIndex, int paragraphIndex, SentenceTreeNavigator navigator) {
			if (paragraphIndex > lastParagrapIndex)
				lastParagrapIndex = paragraphIndex;
			if (!sentenceIncludedWords.containsKey(sentenceIndex)) {
				sentenceIncludedWords.put(sentenceIndex, new HashSet<String>());
			}
			
			
			List<Word> words = navigator.getWords();
			for (Word word : words) {
				if (word.getPOS().isNoun()) {
					includeWord(word, sentenceIndex);
				}
			}
			return this;
		}
		
		private void includeWord(Word word, int sentenceIndex) {
			String lemma = word.getLemma().toLowerCase();
			if (lemma != null) {
				Integer value = wordCounts.get(lemma);
				if (value != null) {
					value = value + 1;
				} else {
					value = 1;
				}
				wordCounts.put(lemma, value);
				
				Set<String> sentenceWords = sentenceIncludedWords.get(sentenceIndex);
				sentenceWords.add(lemma);
				sentenceIncludedWords.put(sentenceIndex, sentenceWords);
			}
		}
		
		public SentenceQuestionDesignsComparator build() {
	        SortedSet<String> sortedLemmas = new TreeSet<>(new Comparator<String>() {
				@Override
                public int compare(String o1, String o2) {					
	                int c = wordCounts.get(o1).compareTo(wordCounts.get(o2));
	                if (c == 0) {
	                	return o1.compareTo(o2);
	                } else {
	                	return -c;
	                }
                }
			});
	        sortedLemmas.addAll(wordCounts.keySet());
			return new SentenceQuestionDesignsComparator(sortedLemmas, sentenceIncludedWords, lastParagrapIndex);
		}
	}
	
	@Override
	public int compare(SentenceQuestionDesigns o1, SentenceQuestionDesigns o2) {
		Set<String> o1Words = sentenceIncludedWords.get(o1.getSentenceIndex());
		Set<String> o2Words = sentenceIncludedWords.get(o2.getSentenceIndex());
		for (String lemma: sortedLemmas) {
			if (o1Words.contains(lemma) && o2Words.contains(lemma)) {
				return o1.getSentenceIndex() - o2.getSentenceIndex();
			}			
			if (o1Words.contains(lemma) && !o2Words.contains(lemma)) {
				return -1;
			}
			if (!o1Words.contains(lemma) && o2Words.contains(lemma)) {
				return 1;
			}
		}
		
		return o1.getSentenceIndex() - o2.getSentenceIndex();
	}
}
