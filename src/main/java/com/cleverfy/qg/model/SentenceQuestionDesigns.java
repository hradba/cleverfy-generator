package com.cleverfy.qg.model;

import java.util.Collection;
import java.util.Collections;

public class SentenceQuestionDesigns {
	
	private String text;
	private int sentenceIndex;
	private Collection<QuestionDesign> questionDesigns;
	
	public SentenceQuestionDesigns(String text, int sentenceIndex, Collection<QuestionDesign> questionDesigns) {
		super();
		this.text = text;
		this.sentenceIndex = sentenceIndex;
		this.questionDesigns = questionDesigns;
	}
	
	public int getSentenceIndex() {
		return sentenceIndex;
	}
	
	public String getText() {
		return text;
	}
	
	public Collection<QuestionDesign> getQuestionDesigns() {
		return Collections.unmodifiableCollection(questionDesigns);
	}
	
}
