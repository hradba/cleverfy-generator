package com.cleverfy.qg.parser.dependencies;

/**
 * Parts of speech enum, created according to https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
 * 
 * @author Radim Hradecký
 *
 */
public enum POS {	
	UNKNOWN("Unknown"),
	
	CC("Coordinating conjunction"),
	CD("Cardinal number"),
	DT("Determiner"),
	EX("Existential there"),
	FW("Foreign word"),
	IN("Preposition or subordinating conjunction"),
	JJ("Adjective"),
	JJR("Adjective, comparative"),
	JJS("Adjective, superlative"),
	LS("List item marker"),
	MD("Modal"),
	NN("Noun, singular or mass"),
	NNS("Noun, plural"),
	NNP("Proper noun, singular"),
	NNPS("Proper noun, plural"),
	PDT("Predeterminer"),
	POS("Possessive ending"),
	PRP("Personal pronoun"),
	PRP$("Possessive pronoun"),
	PUNCT("Punctuation"),
	RB("Adverb"),
	RBR("Adverb, comparative"),
	RBS("Adverb, superlative"),
	RP("Particle"),
	SYM("Symbol"),
	TO("to"),
	UH("Interjection"),
	VB("Verb, base form"),
	VBD("Verb, past tense"),
	VBG("Verb, gerund or present participle"),
	VBN("Verb, past participle"),
	VBP("Verb, non-3rd person singular present"),
	VBZ("Verb, 3rd person singular present"),
	WDT("Wh-determiner"),
	WP("Wh-pronoun"),
	WP$("Possessive wh-pronoun"),
	WRB("Wh-adverb	");
	
	private final String description;
	
	POS(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public boolean isPastVerb() {
		if (this == VBD || this == VBN) return true;
		return false;
	}
	
	public boolean isVerb() {
		if (this == VB || this == VBD || this == VBG || this == VBN || this == VBP || this == VBZ) return true;
		return false;
	}

	public boolean isNoun() {
		if (this == NN || this == NNS || this == NNP || this == NNPS || this == PRP || this == PRP$) return true;
		return false;		
	}
	
	public boolean isAdjective() {
		if (this == JJ || this == JJR || this == JJS) return true;
		return false;
	}

	public boolean isAdverb() {
		if (this == RB || this == RBR || this == RBS) return true;
		return false;
	}
	
	public boolean isPreposition() {
		if (this == IN || this == TO) return true;
		return false;
	}
 
	public boolean isUnknown() {
		if (this == UNKNOWN) return true;
		return false;
	}
	
	public static POS safeValueOf(String value) {
		try {
			return valueOf(value);
		} catch (IllegalArgumentException e) {
			return UNKNOWN;
		}
	}

}
