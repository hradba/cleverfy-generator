package com.cleverfy.qg.parser.dependencies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.semgraph.SemanticGraphFormatter;
import edu.stanford.nlp.util.CoreMap;

public class ParsedText implements Serializable {
	final static Logger logger = LoggerFactory.getLogger(ParsedText.class);
    private static final long serialVersionUID = 8949272724477481545L;

    private List<SentenceTree> sentenceTrees;
	private SemanticGraphFormatter semanticGraphFormatter = new SemanticGraphFormatter();
    
	public ParsedText() {
	    super();
	    sentenceTrees = new ArrayList<>();
    }


	public ParsedText(Annotation document) {
		sentenceTrees = new ArrayList<>();
				
		List<CoreMap> chunkSentences = document.get(SentencesAnnotation.class);
		for (CoreMap chunkSentence : chunkSentences) {
			SemanticGraph deps = chunkSentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
			logger.info(semanticGraphFormatter.formatSemanticGraph(deps));
			sentenceTrees.add(new SentenceTree(chunkSentence, deps));
		}
	}
	
	
	public List<SentenceTree> getSentenceTrees() {
		return Collections.unmodifiableList(sentenceTrees);
	}


	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
