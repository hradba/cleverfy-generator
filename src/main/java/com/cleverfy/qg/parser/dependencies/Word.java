package com.cleverfy.qg.parser.dependencies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.utils.StringUtils;

import edu.stanford.nlp.ling.CoreAnnotations.AfterAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.BeforeAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.IndexAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.OriginalTextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.IndexedWord;

public class Word implements Serializable, Comparable<Word> {
	
	private static final long serialVersionUID = -6403321884581650706L;
	
	final static Logger logger = LoggerFactory.getLogger(Word.class);
	
	private String lemma;
	private String text;
	private String before;
	private String after;
	private NER ner;
	private POS pos;
	private Integer beginOffset;
	private List<Relation> children;	
	

	public Word() {
		
	}
	
	public Word(IndexedWord word, List<Relation> children) {
		init(word.get(LemmaAnnotation.class), word.getString(OriginalTextAnnotation.class),
		        word.getString(BeforeAnnotation.class), word.getString(AfterAnnotation.class),
		        NER.safeValueOf(word.getString(NamedEntityTagAnnotation.class)),
		        POS.safeValueOf(word.get(PartOfSpeechAnnotation.class)),
		        word.get(CharacterOffsetBeginAnnotation.class), word.get(IndexAnnotation.class), children);
	}
	
	private void init(String lemma, String text, String before, String after, NER ner, POS pos, Integer beginOffset, Integer indexAnnotation,
	        List<Relation> children) {
		this.lemma = lemma;
		this.before = before;
		this.after = after;
		this.ner = ner;
		this.pos = pos;
		this.beginOffset = beginOffset;
		this.children = children;
		
		if (indexAnnotation != null && indexAnnotation == 1 && ner != NER.LOCATION && ner != NER.PERSON && ner != NER.ORGANIZATION) {
			this.text = StringUtils.lowerCaseFirstLetter(text);
		} else {
			this.text = text;
		}
	}
	
	public String getLemma() {
		return lemma;
	}
	
	public String getText() {
		return text;
	}
	
	public NER getNER() {
		return ner;
	}
	
	public POS getPOS() {
		return pos;
	}
	
	public Integer getBeginOffset() {
		return beginOffset;
	}
	
	public String getBefore() {
		return before;
	}
	
	public String getAfter() {
		return after;
	}
	
	public boolean hasNegationModifier() {
		if (!getPOS().isVerb()) logger.warn("Negativity tested on non-verb (" + getText() + ")");
		return getChildrenWithRelation(RelationType.NEGATION_MODIFIER).size() > 0;
	}
	
	public boolean inPlural() {
		if (!getPOS().isNoun() && !getPOS().isNoun()) logger.warn("Plurality tested on non-noun (" + getText() + ", " + getPOS() + ")");
		if (getLemma() != null && getLemma().equals(getText())) {
			return true;
		} else {
			return false;
		}
	}
	
	public List<Relation> getChildren() {
		return Collections.unmodifiableList(children);
	}
	
	public boolean hasChildWithRelation(RelationType relationType) {
		for (Relation relation : children) {
			if (relationType == relation.getRelationType()) {
				return true;
			}
		}
		return false;
	}
	
	public Relation getChildWithRelation(RelationType relationType) {
		List<Relation> relations = getChildrenWithRelation(new ArrayList<>(Arrays.asList(relationType)));
		if (relations.size() > 0) return relations.get(0);
		return null;
	}	
	
	public List<Relation> getChildrenWithRelation(RelationType relationType) {
		return getChildrenWithRelation(new ArrayList<>(Arrays.asList(relationType)));		
	}
	
	public List<Relation> getChildrenWithRelation(List<RelationType> relationTypes) {
		List<Relation> relations = new ArrayList<>();
		for (Relation relation : children) {
			if (relationTypes.contains(relation.getRelationType())) {
				relations.add(relation);
			}
		}
		return relations;
	}

	public List<Relation> getRecursiveChildren(RecursiveRelationFilter relationFilter) {
		List<Relation> relations = new ArrayList<>();
		for (Relation relation : children) {
			if (relationFilter.recursionAllowed(relation)) {
				if (relationFilter.relationAccepted(relation)) {
					relations.add(relation);
				} else {
					relations.addAll(relation.getTarget().getRecursiveChildren(relationFilter));
				}
			}
		}
		return relations;
	}

	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	@Override
	public int compareTo(Word o) {
		return this.getBeginOffset() - o.getBeginOffset();
	}
}
