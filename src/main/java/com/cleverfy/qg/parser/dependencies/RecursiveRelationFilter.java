package com.cleverfy.qg.parser.dependencies;

public interface RecursiveRelationFilter extends RelationFilter {

	boolean recursionAllowed(Relation relation);

}
