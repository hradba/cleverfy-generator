package com.cleverfy.qg.parser.dependencies;

/**
 * Named entity tags enum, created according to: http://nlp.stanford.edu/software/CRF-NER.shtml
 * 
 * @author Radim Hradecký
 *
 */
public enum NER {
	
	TIME, LOCATION, ORGANIZATION, PERSON, MONEY, PERCENT, DATE, O, NUMBER, MISC, DURATION, UNKNOWN;
	
	

	public boolean isPerson() {
		if (this == NER.PERSON) {
			return true;
		} else {
			return false;
		}
	}
	

	public boolean isTime() {
		if (this == NER.TIME) {
			return true;
		} else {
			return false;
		}
	}
	

	public boolean isDate() {
		if (this == NER.DATE) {
			return true;
		} else {
			return false;
		}
	}
	

	public boolean isLocation() {
		if (this == NER.LOCATION) {
			return true;
		} else {
			return false;
		}
	}
	
	public static NER safeValueOf(String value) {
		try {
			return valueOf(value);
		} catch (IllegalArgumentException e) {
			return UNKNOWN;
		}
	}
}
