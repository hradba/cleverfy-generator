package com.cleverfy.qg.parser.dependencies;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class Relation implements Serializable {

    private static final long serialVersionUID = 1L;
	private Word target;
	private RelationType relationType;
	
	public Relation() {	 
    }
	
	public Relation(Word target, RelationType relationType) {
	    super();
	    this.target = target;
	    this.relationType = relationType;
    }

	public Word getTarget() {
		return target;
	}

	public RelationType getRelationType() {
		return relationType;
	}
	
	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
