package com.cleverfy.qg.parser.dependencies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SentenceTreeNavigator {
	
	Map<Word, List<Relation>> childrenMap = new HashMap<>();
	Map<Word, Word> parentMap = new HashMap<>();
	List<Word> words = new ArrayList<>();	
	List<Word> verbs = new ArrayList<>();
	SentenceTree sentenceTree;
	
	public SentenceTreeNavigator(SentenceTree sentenceTree) {
		this.sentenceTree = sentenceTree;
		addWord(sentenceTree.getRoot());
		Collections.sort(words);		
    }
	
	public List<Word> getWords() {
		return Collections.unmodifiableList(words);
	}
	
	public boolean hasRelationWithParent(Word word, RelationType relationType) {
		Word parent = parentMap.get(word);
		List<Relation> childrenRelations = parent.getChildren();
		for (Relation childRelation: childrenRelations) {
			if (childRelation.getTarget().equals(word)) {
				if (childRelation.getRelationType() == relationType) {
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}
	
	public List<Relation> getSiblings(Word word) {
		Word parent = parentMap.get(word);
		return parent.getChildren();
	}
	
	public List<Word> getSiblings(Word word, RelationFilter relationFilter) {
		Word parent = parentMap.get(word);		
		List<Relation> children = parent.getChildren();
		List<Word> filteredChildren = new ArrayList<>();
		for (Relation relation: children) {
			if (relationFilter.relationAccepted(relation) && !relation.getTarget().equals(word)) {
				filteredChildren.add(relation.getTarget());
			}
		}
		return filteredChildren;
	}
	
	
	public Word getWord(String text) {
		for (Word word: words) {
			//Case is ignored, because first letter may be lower-cased due to the transfer to questions
			if (word.getText().equalsIgnoreCase(text)) return word;
		}
		throw new IllegalArgumentException("Requested word not found in sentence");
	}

	public List<Word> getVerbs() {
		return verbs;
	}

	public String format() {		
		return sentenceTree.getSourceTree();
	}
	
	public String formatTargetSentenceTree() {
		return sentenceTree.format();
	}
	
	public String text() {
		StringBuilder sb = new StringBuilder();
		for (Word word: words) {
			sb.append(word);
		}
		return sb.toString();
	}
	
	private void addWord(Word word) {
		words.add(word);
		if (word.getPOS().isVerb()) {
			verbs.add(word);
		}
		List<Relation> children = word.getChildren();
		for (Relation childRelation: children) {
			addWord(childRelation.getTarget());
			addRelation(word, childRelation);
		}
	}
	
	
	

	private void addRelation(Word source, Relation relation) {
		if (!childrenMap.containsKey(source)) childrenMap.put(source, new ArrayList<Relation>());		
		childrenMap.get(source).add(relation);
		parentMap.put(relation.getTarget(), source);
	}
	
	
}
