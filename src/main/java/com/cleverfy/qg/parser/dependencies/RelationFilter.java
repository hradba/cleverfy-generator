package com.cleverfy.qg.parser.dependencies;

public interface RelationFilter {
	boolean relationAccepted(Relation relation);
}
