package com.cleverfy.qg.parser.dependencies;


public enum RelationType {
	
	PREDICATE("pred","predicate"),
	AUX_MODIFIER("aux","auxiliary"),
	AUX_PASSIVE_MODIFIER("auxpass","passive auxiliary"),
	COPULA("cop","copula"),
	CONJUNCT("conj","conjunct"),
	COORDINATION("cc","coordination"),
	PUNCTUATION("punct","punctuation"),
	ARGUMENT("arg","argument"),
	SUBJECT("subj","subject"),
	NOMINAL_SUBJECT("nsubj","nominal subject"),
	NOMINAL_PASSIVE_SUBJECT("nsubjpass","nominal passive subject"),
	CLAUSAL_SUBJECT("csubj","clausal subject"),
	CLAUSAL_PASSIVE_SUBJECT("csubjpass","clausal passive subject"),
	COMPLEMENT("comp","complement"),
	OBJECT("obj","object"),
	DIRECT_OBJECT("dobj","direct object"),
	INDIRECT_OBJECT("iobj","indirect object"),
	PREPOSITIONAL_OBJECT("pobj","prepositional object"),
	PREPOSITIONAL_COMPLEMENT("pcomp","prepositional complement"),
	ATTRIBUTIVE("attr","attributive"),
	CLAUSAL_COMPLEMENT("ccomp","clausal complement"),
	XCLAUSAL_COMPLEMENT("xcomp","xclausal complement"),
	RELATIVE("rel","relative"),
	REFERENT("ref","referent"),
	EXPLETIVE("expl","expletive"),
	ADJECTIVAL_COMPLEMENT("acomp","adjectival complement"),
	MODIFIER("mod","modifier"),
	ADV_CLAUSE_MODIFIER("advcl","adverbial clause modifier"),
	RELATIVE_CLAUSE_MODIFIER("rcmod","relative clause modifier"),
	MARKER("mark","marker"),
	ADJECTIVAL_MODIFIER("amod","adjectival modifier"),
	NUMERIC_MODIFIER("num","numeric modifier"),
	NUMBER_MODIFIER("number","compound number modifier"),
	QUANTIFIER_MODIFIER("quantmod","quantifier modifier"),
	NOUN_COMPOUND_MODIFIER("nn","nn modifier"),
	APPOSITIONAL_MODIFIER("appos","appositional modifier"),
	DISCOURSE_ELEMENT("discourse","discourse element"),
	VERBAL_MODIFIER("vmod","verb modifier"),
	ADVERBIAL_MODIFIER("advmod","adverbial modifier"),
	NEGATION_MODIFIER("neg","negation modifier"),
	NP_ADVERBIAL_MODIFIER("npadvmod","noun phrase adverbial modifier"),
	TEMPORAL_MODIFIER("tmod","temporal modifier"),
	MULTI_WORD_EXPRESSION("mwe","multi-word expression"),
	MEASURE_PHRASE("measure","measure-phrase"),
	DETERMINER("det","determiner"),
	PREDETERMINER("predet","predeterminer"),
	PRECONJUNCT("preconj","preconjunct"),
	POSSESSION_MODIFIER("poss","possession modifier"),
	POSSESSIVE_MODIFIER("possessive","possessive modifier"),
	PREPOSITIONAL_MODIFIER("prep","prepositional modifier"),
	PHRASAL_VERB_PARTICLE("prt","phrasal verb particle"),
	PARATAXIS("parataxis","parataxis"),
	GOES_WITH("goeswith","goes with"),
	SEMANTIC_DEPENDENT("sdep","semantic dependent"),
	AGENT("agent","agent"),
	
	UNKNOWN("unkn", "unknown");
	
	private String shortName;
	private String description;
	
	private RelationType(String shortName, String description) {
		this.shortName = shortName;
		this.description = description;
	}
	
	
	
	public String getShortName() {
		return shortName;
	}



	public String getDescription() {
		return description;
	}



	public static RelationType safeValueOf(String shortName) {
		for (RelationType relationType: values()) {
			if (relationType.getShortName().equals(shortName)) return relationType;
		}
		return UNKNOWN;
	}
}
