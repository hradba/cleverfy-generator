package com.cleverfy.qg.parser.dependencies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.semgraph.SemanticGraphFormatter;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.util.CoreMap;

public class SentenceTree implements Serializable {
	
    private static final long serialVersionUID = 1360283779123553111L;

    final static Logger logger = LoggerFactory.getLogger(SentenceTree.class);
	Word root;
	String text;
	String sourceTree;
	SemanticGraphFormatter semanticGraphFormatter = new SemanticGraphFormatter();
	
	public SentenceTree() {
		
	}
	
	public SentenceTree(CoreMap map, SemanticGraph deps) {
		if (deps.getRoots().size() > 1) {
			logger.warn("Sentence with multiple roots parsed, only first taken");
		}		
		root = createDepWord(deps.getFirstRoot(), deps);
		text = map.toString();
		sourceTree = semanticGraphFormatter.formatSemanticGraph(deps); 
	}
	
	public Word getRoot() {
		return root;
	}

	public String getSourceTree() {
		return sourceTree;
	}

	private Word createDepWord(IndexedWord word, SemanticGraph deps) {
		
		List<SemanticGraphEdge> edges = deps.outgoingEdgeList(word);
		List<Relation> children = new ArrayList<>();
		Word copulaChild = null;
		IndexedWord copulaIndexedWord = null;
		for (SemanticGraphEdge edge : edges) {		
			if (!edge.getRelation().equals(EnglishGrammaticalRelations.COPULA)) {
				Word child = createDepWord(edge.getTarget(), deps);			
				children.add(new Relation(child, RelationType.safeValueOf(edge.getRelation().getShortName())));
			} else {
				copulaIndexedWord = edge.getTarget();
				copulaChild = createDepWord(copulaIndexedWord, deps);
			}
		}
		
		if (copulaChild == null) {			
			return new Word(word, children);
		} else {
			children.add(new Relation(new Word(word, copulaChild.getChildren()), RelationType.COPULA));
			return new Word(copulaIndexedWord, children);			
		}
		
	}

	public List<Relation> getOutgoing(Word word) {
		List<Relation> outgoing = word.getChildren();
		if (outgoing != null) {
			return outgoing;
		} else {
			return new ArrayList<>();
		}
	}
	
	public String format() {
		return formatWord(root, new StringBuilder(), "").toString();
	}
	
	public StringBuilder formatWord(Word word, StringBuilder sb, String tabLevel) {
		sb.append("[").append(word.getText()).append("/").append(word.getPOS());
		List<Relation> outgoing = getOutgoing(word);
		if (outgoing.size() > 0) {
			sb.append("\n");
		}
		for (Relation relation: outgoing) {
			sb.append(relation.getRelationType().getShortName()).append(":");
			sb = formatWord(relation.getTarget(), sb, tabLevel + "\t");
		}
		
		sb.append("]");
		return sb;
	}
		
	public String getText() {
		return text;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
