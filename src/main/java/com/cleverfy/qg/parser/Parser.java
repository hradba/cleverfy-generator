package com.cleverfy.qg.parser;

import com.cleverfy.qg.parser.dependencies.ParsedText;

public interface Parser {

	 ParsedText parse(String text);
}
