package com.cleverfy.qg.parser;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.parser.dependencies.ParsedText;

import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class StanfordParser implements Parser {
	final static Logger logger = LoggerFactory.getLogger(StanfordParser.class);
	
	private StanfordCoreNLP pipeline;
		
	public StanfordParser() {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner, depparse");
		//props.put("parse.flags", " -makeCopulaHead");
		pipeline = new StanfordCoreNLP(props);		
    }
	
	@Override
    public ParsedText parse(String inputText) {
		long start = System.currentTimeMillis();
		Annotation document = new Annotation(inputText.toString());					
		pipeline.annotate(document);
		logger.info("Parsing (" + inputText.length() + ") took: " + (System.currentTimeMillis() - start) + " ms");
		ParsedText parsedText = new ParsedText(document);
	    return parsedText;
    }
	
}
