package com.cleverfy.qg.utils;

public class StringUtils {
	
	public static String upperCaseFirstLetter(String s) {
		return Character.toString(s.charAt(0)).toUpperCase() + s.substring(1);
	}

	public static String lowerCaseFirstLetter(String s) {
		return Character.toString(s.charAt(0)).toLowerCase() + s.substring(1);
	}
}
