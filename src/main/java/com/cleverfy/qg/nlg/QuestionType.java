package com.cleverfy.qg.nlg;

public enum QuestionType {

	WHAT("What"),
	WHO("Who"),
	WHERE("Where"),
	WHEN("When");
	
	private String questionStart;
	
	QuestionType(String questionStart) {
		this.questionStart = questionStart;
	}

	public String getQuestionStart() {
		return questionStart;
	}
	
	
}
