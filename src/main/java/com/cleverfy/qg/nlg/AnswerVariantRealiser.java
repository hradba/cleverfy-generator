package com.cleverfy.qg.nlg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.extjwnl.JWNLException;
import net.sf.extjwnl.data.IndexWord;
import net.sf.extjwnl.data.POS;
import net.sf.extjwnl.data.PointerUtils;
import net.sf.extjwnl.data.Synset;
import net.sf.extjwnl.data.list.PointerTargetNode;
import net.sf.extjwnl.data.list.PointerTargetNodeList;
import net.sf.extjwnl.dictionary.Dictionary;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.expand.ExpandedWord;
import com.cleverfy.qg.parser.dependencies.NER;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;
import com.cleverfy.qg.utils.StringUtils;

public class AnswerVariantRealiser {
	
	private Dictionary dictionary;
	final static Logger logger = LoggerFactory.getLogger(AnswerVariantRealiser.class);
	private static final Pattern NUMBER_PATTERN = Pattern.compile("(?<number>\\d+)");
	
	private Random random = new Random();
	private Map<NER, List<String>> alternatives;
	
	public AnswerVariantRealiser() {
		try {
			dictionary = Dictionary.getDefaultResourceInstance();
		} catch (JWNLException e) {
			logger.error("Dictionary load failed", e);
		}
		alternatives = new HashMap<NER, List<String>>();
		for (NER ner: NER.values()) {
			alternatives.put(ner, new ArrayList<String>());
		}
	}
	
	
	public void findAlternatives(SentenceTreeNavigator chunkTree) {
		NER currentNER = NER.O;
		StringBuilder sb = new StringBuilder();
		
		List<Word> words = chunkTree.getWords();
		for (int i = 0; i < words.size(); i++) {
			Word word = words.get(i);
			NER ner = word.getNER();
			
			if (ner != NER.O) {
				if (ner == currentNER) {
					sb.append(" ").append(word.getText());
				} else {
					if (sb.length() > 0) {
						alternatives.get(currentNER).add(sb.toString());
					}
					sb = new StringBuilder(word.getText());
				}
			} else {				
				if (sb.length() > 0) {
					alternatives.get(currentNER).add(sb.toString());
					sb = new StringBuilder();
				}
			}
			currentNER = ner;
		}
		if (sb.length() > 0) {
			alternatives.get(currentNER).add(sb.toString());
		}
	}
	
	public Collection<String> realiseAnswerVariants(ExpandedWord expandedAnswer) {
		List<Variant> allVariants = new ArrayList<>();
		List<Word> expandedWords = expandedAnswer.getExpanded();
		for (Word indexedWord : expandedWords) {
			allVariants.addAll(generateWordVariants(indexedWord));
			
		}
		
		Collections.sort(allVariants);
		
		Collection<String> variants = new ArrayList<String>();
		for (Variant variant : allVariants) {
			Map<Word, String> changes = new HashMap<>();
			changes.put(variant.getWordToChange(), variant.getText());
			variants.add(expandedAnswer.text(changes));
		}
		return variants;
		
	}
	
	
	private Set<Variant> generateWordVariants(Word indexedWord) {
		Set<Variant> variants = new HashSet<>();
		POS wordPos = POStoWNPOS(indexedWord.getPOS());
		String lemma = indexedWord.getLemma();
		NER ner = indexedWord.getNER();
		if (wordPos != null) {
			try {
				IndexWord indexWord = dictionary.getIndexWord(wordPos, lemma);
				if (indexWord != null) {
					for (Synset s : indexWord.getSenses()) {
						variants.addAll(pointerTargetNodeListToVariants(indexedWord, PointerUtils.getAntonyms(s)));
					}
				}
			} catch (JWNLException e) {
				logger.error("Dictiobary opration failed", e);
			}
		}
		variants.addAll(numberVariants(indexedWord));
		if ((ner == NER.LOCATION || ner == NER.ORGANIZATION || ner == NER.PERSON) && alternatives.get(ner).size() > 0) {
			int id = random.nextInt(alternatives.get(ner).size());
			String text = alternatives.get(ner).get(id);
			variants.add(new Variant(indexedWord, text, 30));
		}
		return variants;
	}
	
	private Set<Variant> numberVariants(Word indexedWord) {
		Set<Variant> variants = new HashSet<>();
		Matcher numberMatcher = NUMBER_PATTERN.matcher(indexedWord.getText());
		while (numberMatcher.find()) {
			long number = Long.parseLong(numberMatcher.group("number"));
			String text = numberMatcher.replaceFirst(String.valueOf(getRandomWithinPercentRange(number, 1)));
			variants.add(new Variant(indexedWord, text, 20));
			text = numberMatcher.replaceFirst(String.valueOf(getRandomWithinPercentRange(number, 5)));
			variants.add(new Variant(indexedWord, text, 30));
		}
		return variants;
	}
	
	private long getRandomWithinPercentRange(long number, int percentRange) {
		long range = (long) Math.ceil(number * ((double) percentRange / 100));
		long value = (long) Math.ceil((random.nextDouble() * range));
		if (random.nextBoolean()) {
			return number + value;
		} else {
			return number - value;
		}
	}
	
	private List<Variant> pointerTargetNodeListToVariants(Word indexedWord, PointerTargetNodeList list) {
		List<Variant> variants = new ArrayList<>();
		Iterator<PointerTargetNode> it = list.iterator();
		while (it.hasNext()) {
			PointerTargetNode n = it.next();
			String text = n.getWord().getLemma();
			String originalText = indexedWord.getText();
			boolean upperCase = Character.isUpperCase(originalText.charAt(0));
			if (upperCase) {
				text = StringUtils.upperCaseFirstLetter(text);
			}
			variants.add(new Variant(indexedWord, text, 10));
		}
		return variants;
	}
	
	public POS POStoWNPOS(com.cleverfy.qg.parser.dependencies.POS pos) {
				
		if (pos != null) {
			if (pos.isAdjective())
				return POS.ADJECTIVE;
			if (pos.isAdverb())
				return POS.ADVERB;
			if (pos.isNoun())
				return POS.NOUN;
			if (pos.isVerb())
				return POS.VERB;
		}
		return null;
	}
	
	class Variant implements Comparable<Variant> {
		private String text;
		private int scoring;
		private Word wordToChange;
		
		public Variant(Word wordToChange, String text, int scoring) {
			super();
			this.text = text;
			this.scoring = scoring;
			this.wordToChange = wordToChange;
		}
		
		public String getText() {
			return text;
		}
		
		public int getScoring() {
			return scoring;
		}
		
		public Word getWordToChange() {
			return wordToChange;
		}
		
		@Override
		public int hashCode() {
			return HashCodeBuilder.reflectionHashCode(this, "scoring");
		}
		
		@Override
		public boolean equals(Object obj) {
			return EqualsBuilder.reflectionEquals(this, obj, "scoring");
		}
		
		@Override
		public int compareTo(Variant o) {
			return CompareToBuilder.reflectionCompare(this, o, "text", "wordToChange");
		}
		
	}
	
}
