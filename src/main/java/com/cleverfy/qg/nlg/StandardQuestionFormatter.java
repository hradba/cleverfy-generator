package com.cleverfy.qg.nlg;

import com.cleverfy.qg.model.QuestionDesign;

public class StandardQuestionFormatter extends QuestionFormatter {

	public StandardQuestionFormatter() {
	   super(" ");
    }
	
	@Override
    public String format(QuestionDesign questionDesign, String... strings) {
		String questionStart = questionDesign.getQuestionType().getQuestionStart();
		return concat(questionStart, concat(strings), "?");
    }
	
}
