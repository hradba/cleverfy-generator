package com.cleverfy.qg.nlg;

import com.cleverfy.qg.model.QuestionDesign;

public abstract class QuestionFormatter {

	private final String delimiter;
	
	
	public QuestionFormatter(String delimiter) {
	    super();
	    this.delimiter = delimiter;
    }

	public abstract String format(QuestionDesign questionDesign, String...strings);
	
	
	protected String concat(String... parts) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parts.length; i++) {
			sb.append(parts[i]);
			if (i + 1 < parts.length && !"".equals(parts[i + 1])) sb.append(delimiter);
		}
		return sb.toString();
	}
	
}
