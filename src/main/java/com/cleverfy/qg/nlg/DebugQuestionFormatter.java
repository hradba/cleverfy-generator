package com.cleverfy.qg.nlg;

import com.cleverfy.qg.model.QuestionDesign;

public class DebugQuestionFormatter extends QuestionFormatter {

	public DebugQuestionFormatter() {
	    super(" | ");
    }
	
	@Override
    public String format(QuestionDesign questionDesign, String... strings) {
		String questionStart = questionDesign.getQuestionType().getQuestionStart();
		int matchId = questionDesign.getMatchId();
		String target = questionDesign.getQuestionTarget().name();
		return concat(questionStart, concat(strings), "? (" + matchId + ", " + target +")");
    }
	
}
