package com.cleverfy.qg.nlg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cleverfy.qg.expand.ExpandedVerbWord;
import com.cleverfy.qg.expand.ExpandedVerbWord.Tense;
import com.cleverfy.qg.model.QuestionDesign;
import com.cleverfy.qg.model.QuestionTarget;
import com.cleverfy.qg.parser.dependencies.Word;


public class QuestionRealiser {
	
	private static final List<String> VERBS_WITHOUT_DO_AUXVERB = new ArrayList<String>(Arrays.asList(new String[] {"be"/*, "have"*/}));	
	private QuestionFormatter formatter;
	
	public QuestionRealiser() {		
	    super();
	    init(new StandardQuestionFormatter());
    }

	public QuestionRealiser(QuestionFormatter questionFormatter) {		
	    super();
	    init(questionFormatter);
	    
    }	
	
	private void init(QuestionFormatter questionFormatter) {
		this.formatter = questionFormatter;
	}
	

	public String realise(QuestionDesign questionDesign) {
		String exSubject = questionDesign.getSubject().text();
		String exObject = questionDesign.getObject().text();
		ExpandedVerbWord exVerb = questionDesign.getVerb();
		String negation = (exVerb.isNegation()) ? "not" : ""; 
		String ext = questionDesign.getExt() != null ? questionDesign.getExt().text() : "";
		String endingPrep = questionDesign.getEndingPrep();
		
		if (questionDesign.getQuestionTarget() == QuestionTarget.SUBJECT) {			
			if (exVerb.hasOwnMainAuxVerb()) {
				String aux = exVerb.getMainAuxVerb().getText();
				return formatter.format(questionDesign, aux, negation, exVerb.text(false, false), exObject, endingPrep);
			} else {				
				return formatter.format(questionDesign, exVerb.text(false, false), exObject, endingPrep);	
			}			
		} else if (questionDesign.getQuestionTarget() == QuestionTarget.OBJECT) {
			
			if (exVerb.hasOwnMainAuxVerb()) {
				String aux = exVerb.getMainAuxVerb().getText();
				return formatter.format(questionDesign, aux, negation, exSubject, exVerb.text(false, false), ext, endingPrep);
			} else {
				Word mainVerb = exVerb.getMainVerb();
				if (VERBS_WITHOUT_DO_AUXVERB.contains(mainVerb.getLemma())) {
					return formatter.format(questionDesign, exVerb.text(false, false), exSubject, ext, endingPrep);	
				} else {					
					String aux = getAuxVerb(exVerb.isMainVerbThirdPerson(), exVerb.getTense() == Tense.PAST, exVerb.isPlural());			
					return formatter.format(questionDesign, aux, negation, exSubject, exVerb.text(true, false), ext, endingPrep);					
				}							
			}
		}				
		return null;
	}


	private String getAuxVerb(boolean thirdPerson, boolean isPast, boolean isPlural) {
		if (isPast) return "did";
		if (thirdPerson && !isPlural) {
			return "does";
		} else {
			return "do";
		}
	}
	
}
