package com.cleverfy.qg;

import java.util.Collection;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Question generated from input text. To make understanding of question easier, it contains context to introduce
 * question. Question also contains correct answer to the question, question importance (experimental) and possible
 * incorrect answer variants (highly-experimental).
 * 
 * @author Radim Hradecký
 *
 */
public class Question {
	
	private String context;
	private String question;
	private String answer;
	private long importanceOrder;
	private Collection<String> answerVariants;
	
	public Question(String context, String question, String answer, long importanceOrder,
	        Collection<String> answerVariants) {
		super();
		this.context = context;
		this.question = question;
		this.answer = answer;
		this.importanceOrder = importanceOrder;
		this.answerVariants = answerVariants;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public Collection<String> getAnswerVariants() {
		return answerVariants;
	}
	
	public String getContext() {
		return context;
	}
	
	public long getImportanceOrder() {
		return importanceOrder;
	}
	
	/**
	 * @return human-friendly representation of whole question
	 */
	public String prettyPrint() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n").append(question).append(" (").append(importanceOrder).append(") Given that: ").append(context)
		        .append(")\n");
		sb.append("   ").append(answer).append(" (X)");
		for (String answerVariant : answerVariants) {
			sb.append("\n   ").append(answerVariant);
		}
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
}
