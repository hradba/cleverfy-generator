package com.cleverfy.qg;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Class for representation of highlighted range of text
 * 
 * @author Radim Hradecký
 *
 */
public class HighlightRange {
	private long start;
	private long end;
	
	public HighlightRange(long start, long end) {
        super();
        this.start = start;
        this.end = end;
    }

	public long getStart() {
		return start;
	}

	public long getEnd() {
		return end;
	}
	
	
	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
}