package com.cleverfy.qg.decompose;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import edu.stanford.nlp.trees.GrammaticalRelation;

public class ObjectMatcher {

	
	public static List<GrammaticalRelation> OBJECT_RELATIONS;
	
	static {
		OBJECT_RELATIONS = new ArrayList<GrammaticalRelation>();
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.COMPLEMENT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.ADJECTIVAL_COMPLEMENT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.CLAUSAL_COMPLEMENT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.XCLAUSAL_COMPLEMENT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.OBJECT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.DIRECT_OBJECT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.INDIRECT_OBJECT);
		OBJECT_RELATIONS.add(EnglishGrammaticalRelations.PREPOSITIONAL_OBJECT);				
				
	}
	
	public Set<IndexedWord> getObjectSiblings(IndexedWord object, SemanticGraph deps) {
		IndexedWord parent = deps.getParent(object);
		
		Set<IndexedWord> objects = new HashSet<>();
		List<SemanticGraphEdge> edges = deps.getOutEdgesSorted(parent);
		for (SemanticGraphEdge edge: edges) {
			if (edge.getTarget() == object) continue;
			if (OBJECT_RELATIONS.contains(edge.getRelation())) {
				objects.add(edge.getTarget());
			}
			if (edge.getRelation().equals(EnglishGrammaticalRelations.PREPOSITIONAL_MODIFIER)) {
				if (deps.hasChildWithReln(edge.getTarget(), EnglishGrammaticalRelations.PREPOSITIONAL_OBJECT)) {
					objects.add(edge.getTarget());
				}
			}
		}
		return objects;
		
	}
	
	public boolean isObject(IndexedWord word, SemanticGraph deps) {
		List<SemanticGraphEdge> edges = deps.getIncomingEdgesSorted(word);
		for (SemanticGraphEdge edge: edges) {
			if (OBJECT_RELATIONS.contains(edge.getRelation())) {
				return true;
			}
		}
		return false;
	}
}
