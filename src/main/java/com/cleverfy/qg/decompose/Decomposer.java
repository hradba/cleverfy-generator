package com.cleverfy.qg.decompose;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.parser.dependencies.RecursiveRelationFilter;
import com.cleverfy.qg.parser.dependencies.Relation;
import com.cleverfy.qg.parser.dependencies.RelationFilter;
import com.cleverfy.qg.parser.dependencies.RelationType;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

public class Decomposer {
	
	final static Logger logger = LoggerFactory.getLogger(Decomposer.class);
	
	public Decomposer() {
		
	}
	
	/**
	 * Object could be optional, but it's a question, if questions without object are worth generating. If semgrex allow
	 * optional object, null object generates sometimes strange question. It should rather test if there is an object in
	 * sentence. If not, try optional, if yes, it is required -> TBD
	 */
	//private static final String OBJ = "[ >>comp {}=obj | >>acomp {}=obj | >>ccomp {}=obj | >>xcomp {}=obj | >>obj {}=obj | >>dobj {}=obj | >>iobj {}=obj | >>prep ({}=obj > pobj {})]";
	//private static final String SUBJ = "[>subj {}=subj | >nsubj {}=subj | >nsubjpass {}=subj | >csubj {}=subj | >csubjpass {}=subj]";

	private static final List<RelationType> SUBJECT_RELATIONS = new ArrayList<>();
	
	static {		
		SUBJECT_RELATIONS.add(RelationType.SUBJECT);
		SUBJECT_RELATIONS.add(RelationType.NOMINAL_SUBJECT);
		SUBJECT_RELATIONS.add(RelationType.NOMINAL_PASSIVE_SUBJECT);
		SUBJECT_RELATIONS.add(RelationType.CLAUSAL_SUBJECT);
		SUBJECT_RELATIONS.add(RelationType.CLAUSAL_PASSIVE_SUBJECT);
	}
	
	private static class ObjectRelationFilter implements RelationFilter {
		private static final Map<RelationType, RelationType> OBJECT_RELATIONS = new HashMap<>();
	
		static {
			OBJECT_RELATIONS.put(RelationType.OBJECT, null);
			OBJECT_RELATIONS.put(RelationType.DIRECT_OBJECT, null);
			OBJECT_RELATIONS.put(RelationType.INDIRECT_OBJECT, null);
			OBJECT_RELATIONS.put(RelationType.PREPOSITIONAL_MODIFIER, RelationType.PREPOSITIONAL_OBJECT);
		}
		
		@Override
		public boolean relationAccepted(Relation relation) {
			if (OBJECT_RELATIONS.containsKey(relation.getRelationType())) {
				RelationType subRelation = OBJECT_RELATIONS.get(relation.getRelationType());
				if (subRelation == null) {
					return true;
				} else {
					return relation.getTarget().hasChildWithRelation(subRelation);
				}
			} else {
				return false;
			}
		}
	}; 
	
	//#1 a #4
	private static class RecursiveObjectRelationFilter extends ObjectRelationFilter implements RecursiveRelationFilter {

		@Override
        public boolean recursionAllowed(Relation relation) {
			return (!relation.getTarget().getPOS().isVerb() && !SUBJECT_RELATIONS.contains(relation.getRelationType()));
        }
	}; 
	
	
	
	public Set<DecomposedSentence> decompose(SentenceTreeNavigator navigator) {
		Set<DecomposedSentence> decomposedSentences = new LinkedHashSet<>();
		
		List<Word> verbs = navigator.getVerbs();
		int matchId = 0;
		for (Word verb: verbs) {
			List<Relation> subjectRelations = verb.getChildrenWithRelation(SUBJECT_RELATIONS);
			
			//#3
			List<Relation> objectRelations = verb.getRecursiveChildren(new RecursiveObjectRelationFilter());
			
			for (Relation subjectRelation: subjectRelations) {
				for (Relation objectRelation: objectRelations) {
					DecomposedSentence decomposedSentence = extract(matchId, verb, subjectRelation.getTarget(), objectRelation.getTarget(), navigator);
					decomposedSentences.add(decomposedSentence);
					logger.debug("Match(" + matchId + "): " +  decomposedSentence.prettyPrint());
					matchId++;
					
				}
			}
		}
		
		return decomposedSentences;
	}
	
    private DecomposedSentence extract(int matchId, Word verb, Word subject, Word object, SentenceTreeNavigator navigator) {	
    	/*
		ExpandedWord subject = new ExpandedWord(subjectWord, Expander.defensiveExpandWord(subjectWord, deps));
		ExpandedWord object = new ExpandedWord(objectWord, Expander.expandObject(verbWord, objectWord, deps));
		List<ExpandedWord> objectSiblings = Expander.getObjectSiblings(objectWord, deps);
		VerbWords verb = Expander.expandVerb(verbWord, subjectWord, objectWord, deps, new HashSet<>(Arrays.asList(subjectWord, objectWord)));
    	ExpandedWord exSubject = new ExpandedWord(subject, null);
    	ExpandedWord exObject = new ExpandedWord(object, null);    	
    	VerbWords verbWords = null;*/
    	
    	//#8
    	List<Word> objectSiblings = navigator.getSiblings(object, new ObjectRelationFilter());
		DecomposedSentence decomposedSentence = new DecomposedSentence(matchId, subject, object, objectSiblings, verb);
		return decomposedSentence;    			
	}
}
