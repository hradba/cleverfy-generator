package com.cleverfy.qg.decompose;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.cleverfy.qg.parser.dependencies.Word;


public class DecomposedSentence {

	private int matchId;	
	private Word subject;
	private Word object;
	private List<Word> objectSiblings;
	private Word verb;

	

	
		
	public DecomposedSentence(int matchId, Word subject, Word object, List<Word> objectSiblings, Word verb) {
	    super();
	    this.subject = subject;
	    this.object = object;
	    this.objectSiblings = objectSiblings;
	    this.verb = verb;
	    this.matchId = matchId;
    }

	public Word getSubject() {
		return subject;
	}

	public Word getObject() {
		return object;
	}

	public Word getVerb() {
		return verb;
	}
	
	

	public int getMatchId() {
		return matchId;
	}
	
	

	public List<Word> getObjectSiblings() {
		return objectSiblings;
	}

	public String prettyPrint() {
		StringBuilder sb = new StringBuilder();		
		sb.append("verb=").append(verb != null ? verb.getText() : "").append(",subject=").append(subject.getText()).append(",object=").append(object.getText());
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
	    return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public int hashCode() {
	    return HashCodeBuilder.reflectionHashCode(this);
	}
	
	@Override
	public String toString() {
	    return ToStringBuilder.reflectionToString(this);
	}
}
