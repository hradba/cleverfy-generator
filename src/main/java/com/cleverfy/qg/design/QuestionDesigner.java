package com.cleverfy.qg.design;

import java.util.ArrayList;
import java.util.List;

import com.cleverfy.qg.decompose.DecomposedSentence;
import com.cleverfy.qg.expand.ExpandedVerbWord;
import com.cleverfy.qg.expand.ExpandedWord;
import com.cleverfy.qg.expand.Expander;
import com.cleverfy.qg.expand.VerbExpander;
import com.cleverfy.qg.model.QuestionDesign;
import com.cleverfy.qg.model.QuestionTarget;
import com.cleverfy.qg.nlg.QuestionType;
import com.cleverfy.qg.parser.dependencies.Relation;
import com.cleverfy.qg.parser.dependencies.RelationType;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.Word;

public class QuestionDesigner {
	private Expander expander;
	private VerbExpander verbExpander;
	
	public QuestionDesigner(Expander expander, VerbExpander verbExpander) {
		this.expander = expander;
		this.verbExpander = verbExpander;
	}
	
	public List<QuestionDesign> design(DecomposedSentence decomposedSentence, SentenceTreeNavigator navigator) {
		Word verb = decomposedSentence.getVerb();
		Word subject = decomposedSentence.getSubject();
		List<Word> objectSiblings = decomposedSentence.getObjectSiblings();
		Word object = decomposedSentence.getObject();
		int matchId = decomposedSentence.getMatchId();
		//transferNERIfPossible(verb, subject, object);
		
		ExpandedWord exSubject = expander.expandWord(subject, navigator);
		ExpandedWord exObject = expander.expandWord(object, navigator);
		ExpandedWord exObjectSiblings = expander.expandWords(objectSiblings, navigator);
		ExpandedVerbWord exVerb = verbExpander.expandVerbWord(verb, subject, navigator);		
		
		List<QuestionDesign> questionDesigns = new ArrayList<QuestionDesign>();
		
		// #9
		if (object != null) {
			String endingPrep = getEndingPrep(object);
			QuestionType questionType = getQuestionType(object, navigator);
			ExpandedWord ext = exObjectSiblings;
			questionDesigns.add(new QuestionDesign(matchId, questionType, QuestionTarget.OBJECT, exSubject, exObject, exVerb, ext, endingPrep));
		}
		
		ExpandedWord completeObject = exObject.merge(exObjectSiblings);
		QuestionType questionType = getQuestionType(subject, navigator);
		String endingPrep = getEndingPrep(subject);
		questionDesigns.add(new QuestionDesign(matchId, questionType, QuestionTarget.SUBJECT, exSubject, completeObject, exVerb, endingPrep));
		
		return questionDesigns;
	}
	
/*
	private void transferNERIfPossible(VerbWords verb, Word subject, Word object) {
		if (verb.getMainVerb().getLemma().equals("be") && object != null) {
			String subjectTag = subject.getString(NamedEntityTagAnnotation.class);
			String objectTag = object.get(NamedEntityTagAnnotation.class);
			subject.set(NamedEntityTagAnnotation.class, NER.valueOf(subjectTag) == NER.O ? objectTag : subjectTag);
			object.set(NamedEntityTagAnnotation.class, NER.valueOf(objectTag) == NER.O ? subjectTag : objectTag);
		}
	}
	*/
	// #5
	private QuestionType getQuestionType(Word completeTargetWord, SentenceTreeNavigator navigator) {
		Word answerWithoutPrep = getPrepTarget(completeTargetWord, navigator);
		if (answerWithoutPrep.getNER().isPerson()) {
			return QuestionType.WHO;
		}
		if (answerWithoutPrep.getNER().isLocation() && navigator.hasRelationWithParent(completeTargetWord, RelationType.PREPOSITIONAL_MODIFIER)) {
			String lemma = completeTargetWord.getLemma();
			if (lemma.equals("on") || lemma.equals("in") || lemma.equals("at") || lemma.equals("over")
			        || lemma.equals("to")) {
				return QuestionType.WHERE;
			}
			
		}
		if (answerWithoutPrep.getNER().isTime() || answerWithoutPrep.getNER().isDate()) {
			return QuestionType.WHEN;
		}
		return QuestionType.WHAT;
	}
	
	private Word getPrepTarget(Word word, SentenceTreeNavigator navigator) {
		
		if (navigator.hasRelationWithParent(word, RelationType.PREPOSITIONAL_MODIFIER)) {
			Relation relation = word.getChildWithRelation(RelationType.PREPOSITIONAL_OBJECT);
			if (relation != null) {
				return relation.getTarget();
			} else {
				return word;
			}
		} else {
			return word;
		}
	}
	
	private String getEndingPrep(Word word) {
		if (word.getPOS().isPreposition()) {
			return word.getText();
		}
		return "";
	}
	
}
