package com.cleverfy.qg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for representation of input text of question generation system. In order to generate valid question ordering,
 * it's useful to provide information about text separation into paragraphs
 * 
 * @author Radim Hradecký
 *
 */
public class Text {
	
	private List<Paragraph> paragraphs;
	
	public Text() {
		this.paragraphs = new ArrayList<Paragraph>();
	}
	
	/**
	 * Simple factory method to create Text instance
	 * @param inputText with paragraph-per-line format
	 * @return Text instance based on given input text
	 */
	public static Text valueOf(String inputText) {
		Text text = new Text();
		String[] splited = inputText.split("\r?\n");
		for (String split : splited) {
			text.addParagraph(new Paragraph(split, new ArrayList<HighlightRange>()));
		}
		return text;
	}
	
	public List<Paragraph> getParagraphs() {
		return Collections.unmodifiableList(paragraphs);
	}
	
	private void addParagraph(Paragraph paragraph) {
		paragraphs.add(paragraph);
	}
	
}
