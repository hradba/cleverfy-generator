package com.cleverfy.qg;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.nlg.DebugQuestionFormatter;
import com.cleverfy.qg.parser.FileMockParser;

public class ITWw1QuestionGeneratorTest {
	final static Logger logger = LoggerFactory.getLogger(ITWw1QuestionGeneratorTest.class);

	QuestionGenerator questionGenerator;
	
	@Before
	public void before() {
		questionGenerator = new QuestionGenerator(new DebugQuestionFormatter(), new FileMockParser("ww1-tests.txt"));
	}
	
	@Test
	public void test() {
		questionGenerator.generate("");
	}
}
