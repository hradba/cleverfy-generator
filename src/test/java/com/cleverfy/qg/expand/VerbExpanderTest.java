package com.cleverfy.qg.expand;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.cleverfy.qg.expand.ExpandedVerbWord.Tense;
import com.cleverfy.qg.parser.FileMockParser;
import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.cleverfy.qg.parser.dependencies.Word;

public class VerbExpanderTest {
	
	VerbExpander verbExpander;
	SentenceTreeNavigator ILiveInIreland;
	SentenceTreeNavigator IHaveBeenLivingHereAllMyLife;
	SentenceTreeNavigator ILivedInIreland;
	
	@Before
	public void before() {
		verbExpander = new VerbExpander();
		Parser parser = new FileMockParser("verb-expander-tests.txt");
		ParsedText parsedText = parser.parse("");		
		ILiveInIreland = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(1));
		IHaveBeenLivingHereAllMyLife = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(2));
		ILivedInIreland = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(3));
	}
	
	@Test
	public void simpleVerbShouldBecomeMainVerbAfterExpansion() {		
		Word I = ILiveInIreland.getWord("I");
		Word live = ILiveInIreland.getWord("live");		
		ExpandedVerbWord expandedVerbWord = verbExpander.expandVerbWord(live, I, ILiveInIreland);
				
		assertEquals("live", expandedVerbWord.getMainVerb().getText());
		assertNull(expandedVerbWord.getMainAuxVerb());	
	}
	
	@Test
	public void expandedVerbShouldContainMainVerbAllAuxVerbAndMainAuxVerb() {
		Word I = IHaveBeenLivingHereAllMyLife.getWord("I");
		Word living = IHaveBeenLivingHereAllMyLife.getWord("living");		
		ExpandedVerbWord expandedVerbWord = verbExpander.expandVerbWord(living, I, IHaveBeenLivingHereAllMyLife);
		assertEquals("living", expandedVerbWord.getMainVerb().getText());
		assertEquals("have", expandedVerbWord.getMainAuxVerb().getText());
		
		assertThat(expandedVerbWord.getExpanded(), hasItem(hasProperty("text", equalTo("been"))));
		assertThat(expandedVerbWord.getExpanded(), hasItem(hasProperty("text", equalTo("have"))));
		assertThat(expandedVerbWord.getExpanded(), hasItem(hasProperty("text", equalTo("living"))));		
	}
	
	@Test
	public void presentTenseVerbShouldMakeExpandedVerbWordPresent() {
		Word I = ILiveInIreland.getWord("I");
		Word live = ILiveInIreland.getWord("live");		
		ExpandedVerbWord expandedVerbWord = verbExpander.expandVerbWord(live, I, ILiveInIreland);

		assertEquals(Tense.PRESENT, expandedVerbWord.getTense());
	}

	@Test
	public void pastTenseVerbShouldMakeExpandedVerbWordPresent() {
		Word I = ILivedInIreland.getWord("I");
		Word live = ILivedInIreland.getWord("lived");		
		ExpandedVerbWord expandedVerbWord = verbExpander.expandVerbWord(live, I, ILivedInIreland);

		assertEquals(Tense.PAST, expandedVerbWord.getTense());
	}	

}
