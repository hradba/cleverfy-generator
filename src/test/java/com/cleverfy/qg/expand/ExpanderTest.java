package com.cleverfy.qg.expand;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.cleverfy.qg.parser.FileMockParser;
import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.cleverfy.qg.parser.dependencies.Word;

public class ExpanderTest {
	
	Expander expander;
	ParsedText parsedText;
	
	@Before
	public void before() {
		expander = new Expander();
		Parser parser = new FileMockParser("expander-tests.txt");
		parsedText = parser.parse("");
		
	}
	
	@Test
	public void wordWithNoChildrenShouldHaveOnlyItselfInExpandedList() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(1));
		Word peter = navigator.getWord("Peter");
		ExpandedWord expandedPeter = expander.expandWord(peter, navigator);
		assertEquals(1, expandedPeter.getExpanded().size());
		assertEquals(peter, expandedPeter.getExpanded().get(0));
		
	}

	@Test
	public void expansionOfMultipleWordsShouldUnionTheirChildren() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(1));
		Word peter = navigator.getWord("Peter");
		Word car = navigator.getWord("car");
		Word a = navigator.getWord("a");
		ExpandedWord expandedUnion = expander.expandWords(Arrays.asList(peter, car), navigator);
		assertEquals(3, expandedUnion.getExpanded().size());
		assertEquals(peter, expandedUnion.getExpanded().get(0));
		assertEquals(car, expandedUnion.getExpanded().get(1));
		assertEquals(a, expandedUnion.getExpanded().get(2));
		
	}
	
	
	@Test
	public void expandedWordShouldHaveBeenRootOfExpandedWord() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(1));
		Word peter = navigator.getWord("Peter");
		ExpandedWord expandedPeter = expander.expandWord(peter, navigator);
		assertEquals(peter, expandedPeter.getRoot());
	}
	
	// #6 Test
	@Test
	public void expansionShouldStopOnPrepositionWhoseChildIsVerbInPrepositionalObjectRelation() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(2));
		Word victory = navigator.getWord("victory");
		ExpandedWord expandedVictory = expander.expandWord(victory, navigator);
		assertEquals("a massive German victory", expandedVictory.text());
	}
	
	// #11 Test
	@Test
	public void expansionShouldExpandVerbWhenIsInAdverbModifierRelationship() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(3));
		Word fronts = navigator.getWord("fronts");
		ExpandedWord expandedVictory = expander.expandWord(fronts, navigator);
		assertEquals("opening fronts in the Caucasus, Mesopotamia and the Sinai", expandedVictory.text());
	}
	

	// #10 Test
	@Test
	public void whenExcludingSomethingItsPunctuationMustBeExcludedToo() {
		SentenceTreeNavigator navigator = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(4));
		Word powers = navigator.getWord("powers");
		ExpandedWord expandedPowers = expander.expandWord(powers, navigator);
		assertEquals("all the world's economic great powers", expandedPowers.text());
	}
}
