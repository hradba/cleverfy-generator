package com.cleverfy.qg.decompose;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.parser.FileMockParser;
import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.dependencies.SentenceTree;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.ParsedText;

public class DecomposerTest {
	final static Logger logger = LoggerFactory.getLogger(DecomposerTest.class);
	
	Decomposer decomposer;
	ParsedText parsedText;

	@Before
	public void before() {
		decomposer = new Decomposer();
		Parser parser = new FileMockParser("decomposer-tests.txt");
		parsedText = parser.parse("");
	}
	
	@Test
	public void verbWithOneDirectSubjectAndOneDirectObjectShouldGenerateOneDecompositionOfThose() {		
		SentenceTree tree = parsedText.getSentenceTrees().get(1);
		logger.debug(tree.format());		
		Set<DecomposedSentence> decompositions = decomposer.decompose(new SentenceTreeNavigator(tree));
		assertEquals(1, decompositions.size());
		DecomposedSentence decomposedSentence = decompositions.iterator().next();
		assertEquals("Peter", decomposedSentence.getSubject().getText());
		assertEquals("car", decomposedSentence.getObject().getText());
		assertEquals("has", decomposedSentence.getVerb().getText());
	}
	
	//Test #1
	@Test
	public void decomposedObjectMustBelongToDecomposedVerb_NoOtherVerbMustBeBetween() {
		SentenceTree tree = parsedText.getSentenceTrees().get(2);
		logger.debug(tree.format());
		Set<DecomposedSentence> decompositions = decomposer.decompose(new SentenceTreeNavigator(tree));
		assertEquals(1, decompositions.size());
		DecomposedSentence decomposedSentence = decompositions.iterator().next();
		
		assertEquals("War", decomposedSentence.getSubject().getText());
		assertEquals("on", decomposedSentence.getObject().getText());
		assertEquals("began", decomposedSentence.getVerb().getText());
		
		assertNotEquals("until", decomposedSentence.getObject().getText());				
	}
	
	//Test #4
	@Test
	public void subjectChildIsNotObjectCandidate() {
		SentenceTree tree = parsedText.getSentenceTrees().get(3);
		logger.debug(tree.format());
		Set<DecomposedSentence> decompositions = decomposer.decompose(new SentenceTreeNavigator(tree));
		assertEquals(1, decompositions.size());
		DecomposedSentence decomposedSentence = decompositions.iterator().next();
		assertNotEquals("of", decomposedSentence.getObject().getText());
		
	}

	//Test #5
	@Test
	public void objectsPrepositionIsPartOfObject() {
		SentenceTree tree = parsedText.getSentenceTrees().get(2);
		logger.debug(tree.format());
		Set<DecomposedSentence> decompositions = decomposer.decompose(new SentenceTreeNavigator(tree));
		assertEquals(1, decompositions.size());
		DecomposedSentence decomposedSentence = decompositions.iterator().next();
		assertEquals("on", decomposedSentence.getObject().getText());
		
	}	
	
	//Test #8
	@Test
	public void objectsOnTheSameLevelShouldBeDecomposedAsSiblings() {
		SentenceTree tree = parsedText.getSentenceTrees().get(4);
		logger.debug(tree.format());
		Set<DecomposedSentence> decompositions = decomposer.decompose(new SentenceTreeNavigator(tree));
		assertEquals(3, decompositions.size());
		DecomposedSentence decomposedSentence = decompositions.iterator().next();
		assertEquals(2, decomposedSentence.getObjectSiblings().size());
		
	}
}
