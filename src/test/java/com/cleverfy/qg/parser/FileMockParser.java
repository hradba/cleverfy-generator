package com.cleverfy.qg.parser;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FileMockParser implements Parser {
	final static Logger logger = LoggerFactory.getLogger(FileMockParser.class);
	
	
	private ObjectMapper objectMapper;
	private File file;
	
	public FileMockParser(String mockFile) {
		objectMapper = new ObjectMapper();
		URL fileURL = getClass().getResource("/parse-cache/" + mockFile);
		if (fileURL == null) throw new IllegalArgumentException("Mock file not found. Was the FileMockDataProducer executed?");
		file = new File(fileURL.getFile());
    }
	
	@Override
    public ParsedText parse(String text) {
	    try {
	        return objectMapper.readValue(file, ParsedText.class);
        } catch (IOException e) {
	        logger.error("Mock data deserialization failed", e);
	        return null;
        }
    }
		
}
