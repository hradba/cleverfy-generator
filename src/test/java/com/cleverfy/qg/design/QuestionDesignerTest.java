package com.cleverfy.qg.design;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;

import com.cleverfy.qg.decompose.DecomposedSentence;
import com.cleverfy.qg.expand.ExpandedVerbWord;
import com.cleverfy.qg.expand.ExpandedWord;
import com.cleverfy.qg.expand.Expander;
import com.cleverfy.qg.expand.VerbExpander;
import com.cleverfy.qg.model.QuestionDesign;
import com.cleverfy.qg.model.QuestionTarget;
import com.cleverfy.qg.parser.FileMockParser;
import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.dependencies.SentenceTreeNavigator;
import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.cleverfy.qg.parser.dependencies.Word;

public class QuestionDesignerTest {
	QuestionDesigner questionDesigner;
	SentenceTreeNavigator PeterLovesMary;
	
	@Before
	public void before() {
		questionDesigner = new QuestionDesigner(new Expander(), new VerbExpander());
		Parser parser = new FileMockParser("designer-tests.txt");
		ParsedText parsedText = parser.parse("");
		PeterLovesMary = new SentenceTreeNavigator(parsedText.getSentenceTrees().get(1));
	}
	
	@Test
	public void inSubjectQuestionObjectAndVerbShouldBeInQuestionDesign() {
		Word subject = PeterLovesMary.getWord("Peter");
		Word object = PeterLovesMary.getWord("Mary");
		Word verb = PeterLovesMary.getWord("loves");
		DecomposedSentence decomposedSentence = new DecomposedSentence(1, subject, object, new ArrayList<Word>(), verb);
		List<QuestionDesign> questionDesigns = questionDesigner.design(decomposedSentence, PeterLovesMary);		
		assertThat(questionDesigns,
				hasItem(allOf(hasProperty("questionTarget", equalTo(QuestionTarget.SUBJECT)), 
						hasProperty("subject", wordText(equalTo("Peter"))), 
						hasProperty("object", wordText(equalTo("Mary"))),
						hasProperty("verb", verbWordText(equalTo("loves"))))));
	}

	@Test
	public void inObjectQuestionSubjectAndVerbShouldBeInQuestionDesign() {
		Word subject = PeterLovesMary.getWord("Peter");
		Word object = PeterLovesMary.getWord("Mary");
		Word verb = PeterLovesMary.getWord("loves");
		DecomposedSentence decomposedSentence = new DecomposedSentence(1, subject, object, new ArrayList<Word>(), verb);
		List<QuestionDesign> questionDesigns = questionDesigner.design(decomposedSentence, PeterLovesMary);
		assertThat(questionDesigns, 
				hasItem(allOf(hasProperty("questionTarget", equalTo(QuestionTarget.OBJECT)),
						hasProperty("subject", wordText(equalTo("Peter"))),
						hasProperty("object", wordText(equalTo("Mary"))), 
						hasProperty("verb", verbWordText(equalTo("loves"))))));	
	}

	private FeatureMatcher<ExpandedWord, String> wordText(Matcher<String> matcher) {		
	    return new FeatureMatcher<ExpandedWord, String>(matcher, "parsedText", "parsedText") {
	        @Override
	        protected String featureValueOf(ExpandedWord actual) {
	            return actual.text();
	        }
	    };
	}

	private FeatureMatcher<ExpandedVerbWord, String> verbWordText(Matcher<String> matcher) {		
	    return new FeatureMatcher<ExpandedVerbWord, String>(matcher, "parsedText", "parsedText") {
	        @Override
	        protected String featureValueOf(ExpandedVerbWord actual) {
	            return actual.text(false, false);	            
	        }
	    };
	}
}
