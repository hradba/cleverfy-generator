package com.cleverfy.qg.test.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RelationListProducer {

	
	public void produce() {
		//Copy code of EnglishGrammarRelations.java to relations.txt
		String[] lines = TestUtils.getResourceTexts("/texts/relations.txt");
		Pattern p = Pattern.compile(".*GrammaticalRelation.* ([^ ]*) =.*\"([^\"]*)\", \"([^\"]*)\".*");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.contains("public static final GrammaticalRelation ")) {
				String text = line + lines[i+1];
				//System.out.println(parsedText);
				Matcher m = p.matcher(text);
				while (m.find()) {
					System.out.println(m.group(1) + "(\"" + m.group(2) + "\",\""+ m.group(3) + "\"),");
				}
			}
		}
		 
	}
}
