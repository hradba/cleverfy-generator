package com.cleverfy.qg.test.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.cleverfy.qg.Question;
import com.cleverfy.qg.QuestionGenerator;

public class QuestionAssertHelper {
	
	QuestionGenerator questionGenerator;
	
	public QuestionAssertHelper() {
		questionGenerator = new QuestionGenerator();
	}
	
	public void expectExactQuestionsFromText(String text, String... expectedQuestions) {
		Collection<Question> generatedQuestionsWithAnswers = questionGenerator.generate(text);
		Set<String> expectedQuestionsSet = new HashSet<>(Arrays.asList(expectedQuestions));
		Set<String> generatedQuestionsSet = new HashSet<>();
		for (Question question: generatedQuestionsWithAnswers) {
			generatedQuestionsSet.add(question.getQuestion());
		}
		
		printQuestionsAndAnswers(generatedQuestionsWithAnswers);
		assertEquals(expectedQuestionsSet, generatedQuestionsSet);
	}
	
	public void printQuestionsAndAnswers(Collection<Question> questions) {
		for (Question question: questions){
			System.out.println(question.getQuestion() + " / " + question.getAnswer());
		}
	}
}
