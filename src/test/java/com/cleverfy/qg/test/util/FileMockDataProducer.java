package com.cleverfy.qg.test.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.regex.Pattern;

import com.cleverfy.qg.parser.Parser;
import com.cleverfy.qg.parser.StanfordParser;
import com.cleverfy.qg.parser.dependencies.ParsedText;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FileMockDataProducer {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Parser parser = new StanfordParser();
		ObjectMapper objectMapper = new ObjectMapper();
		String command = "";
		do {
			Pattern pattern = Pattern.compile(".*\\\\test-classes\\\\texts.*");
			final Collection<String> list = ResourceList.getResources(false, pattern);
			for (final String name : list) {
				String text = TestUtils.getResourceText(new File(name));
				long start = System.currentTimeMillis();
				ParsedText document = parser.parse(text);
				long time = System.currentTimeMillis() - start;
				System.out.println("Parsing of " + name + " took " + time + " ms");
				String targetName = name.replace("target\\test-classes\\texts", "src\\test\\resources\\parse-cache");
				objectMapper.writeValue(new File(targetName), document);
			}
			
			System.out.print("Enter command (exit/reload):");
			InputStreamReader streamReader = new InputStreamReader(System.in);
		    BufferedReader bufferedReader = new BufferedReader(streamReader);
		    command = bufferedReader.readLine();
			
		} while (!command.equals("exit"));
		
	}
}
