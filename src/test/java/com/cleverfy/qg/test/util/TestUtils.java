package com.cleverfy.qg.test.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cleverfy.qg.Question;

public class TestUtils {
	
	final static Logger logger = LoggerFactory.getLogger(TestUtils.class);
	
	public static void logQuestions(Collection<Question> questions) {
		StringBuilder sb = new StringBuilder();
		sb.append("Generated questions:");
		for (Question question : questions) {
			sb.append(question.prettyPrint());
		}
		logger.debug(sb.toString());
	}
	
	public static String getResourceTextByLine(String resource, int[] lines) {
		try {
			String text = FileUtils.readFileToString(new File(TestUtils.class.getResource(resource).getFile()), "UTF-8");
			String[] texts = text.split("\n");
			StringBuilder sb = new StringBuilder();
			for (int line: lines) {				
				sb.append(texts[line - 1]).append("\n");
			}
			return sb.toString();
		} catch (IOException e) {
			throw new RuntimeException("Failed to load test data", e);
		}
	}
	public static String getResourceText(File file) {
		try {
			return FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException("Failed to load test data", e);
		}
	}

	public static String getResourceText(String resource) {
		try {
			return FileUtils.readFileToString(new File(TestUtils.class.getResource(resource).getFile()), "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException("Failed to load test data", e);
		}
	}
	
	public static String[] getResourceTexts(String resource) {
		try {
			List<String> lines = FileUtils
			        .readLines(new File(TestUtils.class.getResource(resource).getFile()), "UTF-8");
			List<String> texts = new ArrayList<String>();
			for (String line : lines) {
				texts.add(line);
			}
			return texts.toArray(new String[0]);
			
		} catch (IOException e) {
			throw new RuntimeException("Failed to load test data", e);
		}
	}
}
