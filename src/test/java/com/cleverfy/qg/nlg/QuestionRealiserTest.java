package com.cleverfy.qg.nlg;

import org.junit.Before;

public class QuestionRealiserTest {

	QuestionRealiser realiser;
	
	@Before
	public void before() {
		realiser = new QuestionRealiser();
	}
	/*
	@Test 
	public void workVerbThirdPersonPresentTenseQuestionShouldUseDoAuxVerb() {		
		assertEquals("Where does Peter work?", realiser.realise(0, QuestionType.WHERE_OBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("works", "work", true, 0), "in Germany"));
		assertEquals("Who works in Germany?", realiser.realise(0, QuestionType.WHO_SUBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("works", "work", true, 0), "in Germany"));						
	}

	@Test 
	public void workVerbThirdPersonPresentTenseQuestionWithPrepositionShouldUseDoAuxVerb() {		
		assertEquals("What does Peter work on?", realiser.realise(0, QuestionType.WHAT_OBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("works", "work", true, 0).addAuxVerb("on", 1), "homework"));
		assertEquals("Who works on homework?", realiser.realise(0, QuestionType.WHO_SUBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("works", "work", true, 0).addAuxVerb("on", 1), "homework"));						
	}
	
	
	@Test 
	public void workVerbNonThirdPersonPastTenseQuestionShouldUseDidAuxVerb() {		
		assertEquals("Where did I work?", realiser.realise(0, QuestionType.WHERE_OBJECT, "I", VerbWords.create(Tense.PAST, false, false).addMainVerb("worked", "work", false, 0), "in Germany"));
		assertEquals("Who worked in Germany?", realiser.realise(0, QuestionType.WHO_SUBJECT, "I", VerbWords.create(Tense.PAST, false, false).addMainVerb("worked", "work", false, 0), "in Germany"));					
	}
	
	
	@Test 
	public void workVerbNonThirdPersonPresentTenseQuestionShouldUseDoAuxVerb() {		
		assertEquals("Where do I work?", realiser.realise(0, QuestionType.WHERE_OBJECT, "I", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("work", "work", false, 0), "in Germany"));
		assertEquals("Who works in Germany?", realiser.realise(0, QuestionType.WHO_SUBJECT, "I", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("work", "work", false, 0), "in Germany"));						
	}
	

	
	@Test 
	public void workVerbPresentPerfectQuestionUseOwnAuxVerb() {				
		assertEquals("Where has Peter worked?", realiser.realise(0, QuestionType.WHERE_OBJECT, "Peter", VerbWords.create(Tense.PAST, false, false).addMainAuxVerb("has", "have", 0).addMainVerb("worked", "work", true, 1), "in Germany"));
		assertEquals("Who has worked in Germany?", realiser.realise(0, QuestionType.WHO_SUBJECT, "Peter", VerbWords.create(Tense.PAST, false, false).addMainAuxVerb("has", "have", 0).addMainVerb("worked", "work", true, 1), "in Germany"));						
	}
	
	
	@Test
	public void toBeVerbQuestionDontUseAuxVerb() {			
		assertEquals("What is Peter?", realiser.realise(0, QuestionType.WHAT_OBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("is", "be", true, 0), "boy"));		
		assertEquals("Who is boy?", realiser.realise(0, QuestionType.WHO_SUBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("is", "be", true, 0), "boy"));				
	}
	
	@Test
	public void toHaveVerbQuestionDontUseAuxVerb() {
		assertEquals("What does Peter have?", realiser.realise(0, QuestionType.WHAT_OBJECT, "Peter",VerbWords.create(Tense.PRESENT, false, false).addMainVerb("has", "have", true, 0), "sister"));
		assertEquals("Who has sister?", realiser.realise(0, QuestionType.WHO_SUBJECT, "Peter", VerbWords.create(Tense.PRESENT, false, false).addMainVerb("has", "have", true, 0), "sister"));		
	}
*/
}
